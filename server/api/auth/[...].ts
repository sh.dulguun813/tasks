// file: ~/server/api/auth/[...].ts
import { NuxtAuthHandler } from '#auth';
import { default as GithubProvider } from 'next-auth/providers/github'
const runtimeConfig = useRuntimeConfig();

export default NuxtAuthHandler({
    secret: 'my-superb-secret',
    providers: [
        GithubProvider({
            clientId: 'e017a4e1ec4bcf07d9f0',
            clientSecret: '129de714e6c6a7b25348bdad36f269cf3f405530',
        }),
    ],
})