import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'Home',
        components: () => import('./views/Home.vue'),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        name: 'Login',
        components: () => import('./views/Login.vue'),
    },
    {
    path: '/register',
    name: 'Register',
    components: () => import('./views/Register.vue'),
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router